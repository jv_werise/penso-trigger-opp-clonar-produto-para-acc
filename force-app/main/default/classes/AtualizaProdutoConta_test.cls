/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to test the update on the Produtos_Contratados__c Account field 
*   based on its Opportunities
*
*   Calling : AtualizaProdutoConta && OpportunityTrigger
*
* NAME: AtualizaProdutoConta_test
* AUTHOR: João Vitor Ramos                                      DATE: 28/10/2020
*******************************************************************************/
@isTest
public class AtualizaProdutoConta_test {
    @isTest
    private static void atualizandoOportunidade(){
        List<Opportunity> oppList = criarAccOpp();
        List<Opportunity> oppUpdList = new List<Opportunity>();

        for(Opportunity opp : oppList){
            opp.StageName = 'Fechado e ganho';
            oppUpdList.add(opp);
        }

        Test.startTest();
            update oppUpdList;
        Test.stopTest();

        Account acc = [Select Produtos_Contratados__c from account limit 1];
        List<String> strList = acc.Produtos_Contratados__c.split(';');
        System.assertEquals(strList.size(), 2);        
    }

    @isTest
    private static void cancelandoOportunidade(){
        List<Opportunity> oppList = criarAccOpp();
        List<Opportunity> oppUpdList = new List<Opportunity>();

        for(Opportunity opp : oppList){
            opp.StageName = 'Fechado e ganho';
            opp.DATA_CANCELAMENTO__c = Date.today();
            opp.DataTerminoServicos__c = Date.today();
            opp.ComentariosCancelamento__c = 'Cliente cancelou';
            opp.Motivo_Cancelamento__c = 'Outros';
            opp.SolicitanteCancelamento__c = [SELECT Id FROM Contact WHERE AccountId = :opp.AccountId].Id;
            opp.CancelamentoAprovado__c = true;
            oppUpdList.add(opp);
        }

        Test.startTest();
            update oppUpdList;
        Test.stopTest();
        
        Account acc = [Select Produtos_Contratados__c from account limit 1];
        System.assertEquals(acc.Produtos_Contratados__c, null);
    }

    private static List<Opportunity> criarAccOpp(){
        //Lists to handle the logic
        List<Opportunity> oppList = new List<Opportunity>();
        List<Account> accList = new List<Account>();
        List<Account> accConList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();
        Map<String,String> accConIdMap = new Map<String,String>();

        //creating Accounts
        for(Integer i=0; i<3; i++){
            Account acc = new Account();
            acc.Name = 'Test'+i;
            acc.Phone = '1199999999'+i;
            acc.Type = 'Base';
            acc.Industry = 'Banco';
            accList.add(acc);
        }
        insert accList;

        //creating Contacts
        for(Account acc : accList){
            Contact con = new Contact(LastName='Contact'+acc.Name, AccountId=acc.Id);
            conList.add(con);
        }
        Insert conList;

        //populating the map whit Account and its Contact values
        accConList = [SELECT id, (SELECT Id, AccountId FROM Contacts LIMIT 1) FROM Account WHERE Id IN: accList];
        for(Account acc : accConList){
            for(Contact con : acc.Contacts){
                if(acc.Id == con.AccountId){
        	        accConIdMap.put(acc.Id, con.id);
                }  	
            }
        }

        //Creating Opportunities
        for(Account acc : accList){
            for(Integer i=0;i<2;i++){
                Opportunity opp = new Opportunity();
                opp.RecordType = [select id from recordtype where name Like 'Vendas'];
                opp.Name = 't'+(i+1);
                opp.CloseDate = Date.today().addDays(7);
                opp.AccountId = acc.Id;
                opp.Type = 'Novo Cliente';
                opp.Amount = 1;
                opp.StageName = 'Desenvolvimento';
                opp.LeadSource = 'Bing';
                if(i == 0){
                    opp.Produtos_contratados__c = 'Cloud';
                }
                else {
                    opp.Produtos_contratados__c = 'Email';
                }
                oppList.add(opp);
            }
        }
        insert oppList;

        //Macthing the Opportunities with its Contacts
        for(Opportunity opp : oppList){
            ocrList.add(new OpportunityContactRole(ContactId=accConIdMap.get(opp.AccountId),
                        OpportunityId=opp.Id,Role='Decision Maker',IsPrimary=true));
        }
        insert ocrList;

        return oppList;
    }
}