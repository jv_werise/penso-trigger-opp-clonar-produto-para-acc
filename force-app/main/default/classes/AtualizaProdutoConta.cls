/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class to update the Produtos_Contratados__c Account field based on its Opportunities
*
*   Test class : AtualizaProdutoConta_test
*   Called from : OpportunityTrigger
*
* NAME: AtualizaProdutoConta
* AUTHOR: João Vitor Ramos                                      DATE: 28/10/2020
*******************************************************************************/
public class AtualizaProdutoConta {

    public static void AtualizaProdutoContratado(List<Opportunity> opps){
        //Lists to handle the logic
        //Getting the AccountId and quering the Accounts related to opps
        List<Opportunity> oppAccs = [SELECT AccountId FROM Opportunity WHERE Id IN: opps];
        List<Id> accIds = new List<Id>();
        for(Opportunity acc : oppAccs){
            accIds.add(acc.AccountId);
        }           
        List<Account> accList = [SELECT id, Name, Produtos_Contratados__c, 
                                (SELECT Id from Opportunities) FROM Account 
                                WHERE Id IN: accIds];

        //Getting the Opportunity's Id and quering all Opportunities related to accList     
        List<Id> oppIds = new List<Id>();  
        for(Account acc : accList){
           for(Opportunity opp : acc.Opportunities){
               oppIds.add(opp.Id);
           }
        }                       
        List<Opportunity> oppList = [SELECT Id, Name, Produtos_Contratados__c, AccountId, CancelamentoAprovado__c,
                                    StageName FROM Opportunity WHERE Id IN: oppIds];

        //List of accounts to update
        List<Account> accUpdList = new List<Account>();


        //Passing through all opps's related accounts and its opportunities to update the Produtos_Contratados__c field
        for(Account acc : accList){
            List<String> prodList = new List<String>();
            
            for(Opportunity opp : oppList){
                //If the opportunity was canceled,we no more uptade it
                if(opp.CancelamentoAprovado__c == true) Continue;
                if(opp.AccountId == acc.Id && opp.StageName == 'Fechado e ganho'){
                    if(opp.Produtos_Contratados__c == null) Continue;
                    List<String> strs = opp.Produtos_Contratados__c.split(';');
                    for(String str : strs){
                        prodList.add(str);
                    }
                }
            }
            //deduplicating prodList and updating the account field with the new values
            set<String> setProdutos = new set<String>(prodList);
            prodList = new List<String>(setProdutos);
            acc.Produtos_Contratados__c = String.join(prodList, ';');
            accUpdList.add(acc);
        }
        update accUpdList;

        
    }
}
