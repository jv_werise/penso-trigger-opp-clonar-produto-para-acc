/*******************************************************************************
*                               Werise - 2019
*-------------------------------------------------------------------------------
*
*	Trigger on Opportunity SObject for (Insert or Update) && (After or Before)
*
* 	For Insert:
*	TotalOppCampaign.OpportunityBO();
*
*	For Update:
*	TotalOppCampaign.OpportunityBO();
*   AtualizaProdutoConta.AtualizaProdutoContratado();
*
*   TEST CLASS: AtualizaProdutoConta_test
*
* NAME: OpportunityTrigger
* AUTHOR: Jefferson F. Presotto                                DATE: 02/07/2019
* UPDATED: João Vitor Ramos                                    DATE: 28/10/2020
*******************************************************************************/

trigger OpportunityTrigger on Opportunity (before insert, before update, after insert, after update, after delete) {
    
    System.debug('######################## OpportunityTrigger is running now!!! ########################');
    
    //If trigger is insert
    if(Trigger.isInsert){
        
        //before insert
        if(Trigger.isBefore){
            
        }
        
        //after insert
        if(Trigger.isAfter){
            
            Boolean contemMeta = FALSE;
            
            //Update the total amount of Opportunity
            for (Opportunity opp: Trigger.new) {
                if(opp.RecordType.Name == 'Metas') continue;
                TotalOppCampaign.OpportunityBO(opp);
            }
            
            //Refresh the report
            GoalOpportunityCreation.createGoalOpportunity(FALSE);       
        }
    }
    
    //If trigger is update
    else if(Trigger.isUpdate){
        
        //before update
        if(Trigger.isBefore){
            
        }
        
        //after update
        if(Trigger.isAfter){
            Boolean contemMeta = FALSE;
            Boolean isLoop = FALSE;
            
            //Update the total amount of Opportunity
            for (Opportunity opp: Trigger.new) {
                if(opp.RecordType.Name == 'Metas') continue;
                Opportunity oldOpp = Trigger.oldMap.get(opp.ID);
                if(opp.Valor_Mensal_Produtos__c != oldOpp.Valor_Mensal_Produtos__c || 
                   opp.StageName != oldOpp.StageName || 
                   opp.CampaignId != oldOpp.CampaignId ){
                       isLoop = TRUE;
                       TotalOppCampaign.OpportunityBO(opp);
                   }
            }
            
        	if(isLoop){
                //Refresh the report
                GoalOpportunityCreation.createGoalOpportunity(FALSE);
            }


            //Creating new logic to clone the Produtos_contratados__c field
            List<Opportunity> oppFiltered = new List<Opportunity>();

            for(Opportunity opp : Trigger.new){
                if(opp.Produtos_Contratados__c != null && opp.StageName == 'Fechado e ganho'){
                    oppFiltered.add(opp);
                }
            }
            if(!oppFiltered.isEmpty()){
                AtualizaProdutoConta.AtualizaProdutoContratado(oppFiltered);
            }
        }
    }
    
    //If Trigger is delete
    else if(Trigger.isDelete){
        
        //after delete
        if(Trigger.isAfter){
            
            //Refresh the report
            GoalOpportunityCreation.createGoalOpportunity(FALSE);
        }
    }
}